import requests
import xml.etree.ElementTree as ET
import argparse
import sys
from xml.etree.ElementTree import ElementTree

def getPlayerNum(player, knownPlayers):
	if knownPlayers.has_key(player) == False:
		knownPlayers[player] = str(len(knownPlayers.keys()) + 1)
	return str(knownPlayers[player])

def recursiveXMLConvo(conversation, parentElement, authorList):
	print '[*] Adding comment with id: ' + conversation.name
	subelement = ET.SubElement(parentElement, 'Comment')

	linkID_tag = ET.SubElement(subelement, 'ID')
	linkID_tag.text = conversation.name

	author_tag = ET.SubElement(subelement, 'Author')
	idNum = getPlayerNum(conversation.author, authorList)
	author_tag.set('id', idNum)

	body_tag = ET.SubElement(subelement, 'Body')
	body_tag.text = conversation.body

	upvotes_tag = ET.SubElement(subelement, 'Upvotes')
	upvotes_tag.text = str(conversation.upvotes)
	
	replies = ET.SubElement(subelement, 'Replies')
	for subconversation in conversation.replies:
		recursiveXMLConvo(subconversation, replies, authorList)

def generateXMLConvo(conversations, output):
	print '[*] Creating root element...'
	root = ET.Element('Conversations')
	authorDict = {}
	for conversation in conversations:
		recursiveXMLConvo(conversation, root, authorDict)
	elementTree = ElementTree(root)
	elementTree.write(output)
	print '[*] Finished writing XML...'

class CommentTree:
	def __init__(self, node):
		print node['data']['name']
		self.name = node['data']['name']
		self.author = node['data']['author']
		self.body = node['data']['body']
		self.upvotes = node['data']['ups']
		self.replies = self.__getReplies__(node)
	def __getReplies__(self, node):
		if node['data']['replies'] == '':
			return []
		trees = []
		for reply in node['data']['replies']['data']['children']:
			if reply['data'].has_key('body') == False:
				break
			trees.append(CommentTree(reply))
		return trees
	
def parseJSON(json):
	treeList = []
	i = 1
	for comment in json[1]['data']['children']:
		if comment.has_key('data') == False:
			break
		if comment['data'].has_key('link_id') == False:
			break
		treeList.append(CommentTree(comment))
	return treeList
def main(args):
	print '[*] Getting json list of comments...'
	r = requests.get(str(args[1]) + ".json")
	if r.status_code != 200:
		return
	print '[*] Got list of comments...'
	treeList = parseJSON(r.json)
	generateXMLConvo(treeList, args[2])

if __name__ == '__main__':
	main(sys.argv)

